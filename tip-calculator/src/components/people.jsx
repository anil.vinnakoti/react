import React, { Component } from 'react';

class People extends Component {
    render() { 
        return (
        <div className='people'>
            <label>Number of People</label>
            <input className='people-input' type='text'/>
        </div>
        );
    }
}
 
export default People;