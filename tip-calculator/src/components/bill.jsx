import React, { Component } from 'react';

class Bill extends Component {
    render() { 
        return (
        <div className='bill'>
            <label>Bill Amount</label>
            <input className='bill-input' type='text'/>
        </div>
        );
    }
}
 
export default Bill;