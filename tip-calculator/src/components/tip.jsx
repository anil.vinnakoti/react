import React, { Component } from 'react';
import Form from './form';
import TipButton from './tipButton';

const percentages = [5,10,15,25,50];

 class Tip extends Component {

     render() { 
         return (
        <div className='tip-percentage'>
            <label>Tip %</label>
            {percentages.map((item,index) => <TipButton key={index} content={item} tipOnClick={this.props.tipHandler}></TipButton>)}
            <Form placeHolder='Custom %' handler={this.props.customHandler}/>
        </div>
         );
     }
 }

 export default Tip;