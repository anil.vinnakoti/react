import React, { Component } from 'react';

class TipButton extends Component {
    render() { 
        // console.log(this.props);
        return (
            <button onClick={this.props.tipOnClick}>{this.props.content}%</button>
        );
    }
}
 
export default TipButton;